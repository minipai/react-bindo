var _      = require('lodash');
var moment = require('moment');
var faker  = require('faker');

exports.getSales = function() {
  return {
    name: faker.hacker.abbreviation() + ' ' + faker.hacker.noun(),
    price: '$' + faker.random.number(2000),
    sales: faker.random.number(100) + 300,
  };
};

exports.getSalesWorst = function() {
  return {
    name: faker.hacker.abbreviation() + ' ' + faker.hacker.noun(),
    price: '$' + faker.random.number(2000),
    sales: faker.random.number(30),
  };
};

exports.topSales = function() {
  return _.sortBy( _.times(5,  () => exports.getSales() ), 'sales').reverse();
};

exports.bestSales = function() {
  return _.sortBy( _.times(5,  () => exports.getSales() ), 'sales').reverse();
};

exports.worstSales = function() {
  return _.sortBy( _.times(5,  () => exports.getSalesWorst() ), 'sales');
};

exports.salesSummary = function(dates, type) {
  var typeMap = {
    d: 'MMM DD',
    w: 'MMM DD',
    m: 'YYYY MMM',
    q: 'YYYY[/Q]Q',
    y: 'YYYY',
  };

  var createSummary = function(date) {
    return {
      label: moment(date).format(typeMap[type]),
      date: date,
      revenueWeb: faker.random.number(2000) + 1000,
      revenueIndoor: faker.random.number(2000) + 1000,
      orders: faker.random.number(100) + 100,
    };
  };

  return dates.map( date => createSummary(date) );
};