var React = require('react/addons'),
    cx = React.addons.classSet;

var Router = require('react-router-component');
var Link = Router.Link;

var NavLi = React.createClass({
  render: function(){

    var classSet = cx({
      'active': (new RegExp(this.props.active)).test(this.props.url)
    });

    return (
      <li className={classSet}>{ this.props.children } </li>
    );
  }
});

var DefaultLayout = React.createClass({
  render: function() {
    var account = this.props.account;
    var url = this.props.url;
    var accountLink;

    if(account) {
      accountLink = (
          <Link href="/">
            <i className="fa fa-fw fa-user"></i> {account.username}
          </Link>
      );
    }

    var spinner = <h3 className="fixCenter"><span className="label label-default"> Loading... </span></h3>;

    return (
      <div>
        <nav className="navbar navbar-fixed-top navbar-inverse">
          <div className="navbar-inner">
            <div className="container">
              {this.props.loading ? spinner : ''}
              <Link href="/" hotkey="i" className="navbar-brand">
                <img src="/images/bindo-logo.png" alt="bindo" width="71" height="21" />
              </Link>
              <ul className="nav navbar-nav">
                <NavLi active="/$" url={url}>
                  <Link href="/"><i className="fa fa-fw fa-th-large"></i> Dashboard</Link>
                </NavLi>
                <NavLi active="/sales" url={url}>
                  <Link href="/sales"><i className="fa fa-fw fa-suitcase"></i> Sales</Link>
                </NavLi>
                <NavLi active="/x" url={url}>
                  <Link href="/"><i className="fa fa-fw fa-archive"></i> Inventory</Link>
                </NavLi>
                <NavLi active="/x" url={url}>
                  <Link href="/"><i className="fa fa-fw fa-smile-o"></i> Customer</Link>
                </NavLi>
                <NavLi active="/x" url={url}>
                  <Link href="/"><i className="fa fa-fw fa-tag"></i> Products</Link>
                </NavLi>
                <NavLi active="/x" url={url}>
                  <Link href="/"><i className="fa fa-fw fa-user"></i> Employees</Link>
                </NavLi>

              </ul>
              <ul className="nav navbar-nav pull-right">
                <NavLi active="/x" url={url}>{ account ? accountLink : '' }</NavLi>
              </ul>
            </div>
          </div>
        </nav>

        <main id="main">
          { this.props.children }
        </main>
      </div>
    );
  }
});

module.exports = DefaultLayout;