var React = require('react');

var MainLayout = React.createClass({
  render: function() {
    var content;

    switch( this.props.layout) {
      case 'panel':
        content = (
          <div className="panel panel-default">
            <div className="panel-body">
              {this.props.children}
            </div>
          </div>
        );
        break;

      default:
        content = this.props.children;
        break;
    }

    return (
      <main id="content" className={this.props.className || 'container'}>
        {content}
      </main>
    );
  }
});

module.exports = MainLayout;