/** @jsx React.DOM */

var React = require('react');
var Router = require('react-router-component'),
    Link     = Router.Link;

var Pager = React.createClass({

  render: function() {
    var pager = this.props.pager;
    var path = this.props.path;

    var prev, next, pages;

    pages = pager.range.map(function(p) {
      return <Link href={path+'?page='+p} className={p==pager.current?'active':''}>{p}</Link>;
    });

    if(pager.previous) {
      prev = <Link href={path+"?page="+pager.previous}>上一頁</Link>;
    } else {
      prev = <span className="text-muted">上一頁</span>;
    }

    if(pager.next) {
      next = <Link href={path+"?page="+pager.next}>下一頁</Link>;
    } else {
      next = <span className="text-muted">下一頁</span>;
    }

    return (
       <div className="paginate">
          {prev}
          {pages}
          {next}
        </div>
    );
  }
});

module.exports = Pager;
