var React = require('react');

var Toolbar = React.createClass({
  render: function() {
    return (
      <div className="toolbar">
        <div className="container">
          {this.props.children}
        </div>
      </div>
    );
  }
});

module.exports = Toolbar;
