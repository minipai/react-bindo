var React = require('react');

var React = require('react');
var Router = require('react-router-component'),
    Pages    = Router.Pages,
    Page     = Router.Page,
    NotFound = Router.NotFound;

var HomeIndex = require('./home/index.jsx');
var SalesIndex = require( './sales/index.jsx' );
var _404 = React.createClass({
  render: function(){
    return (<p>React 404</p>);
  }
});
var Async = require('react-async');

var reload = require('../stores/page.js').reload;
var pageStore = require('../stores/page.js').pageStore;

var routes = React.createClass({
  mixins: [Async.Mixin],

  onBeforeNavigation: function(){
    this.setProps({ loading: true });

    reload();
  },

  getInitialStateAsync: function(cb) {
    pageStore.listen(function(data) {
      cb(null, data);
    });
  },

  componentDidMount: function() {
    var _this = this;
    pageStore.listen(function(result) {

      if(_this.isMounted()) {
        if(_this.props.loading) {
          window.scrollTo(0, 0);
        }
        _this.replaceProps(result);
      }
    });
  },

  render: function() {
    var activePath = this.props.path;
    delete this.props.path;

    return (
      <html>
        <head>
          <title>Bindo Admin</title>
          <link href='//fonts.googleapis.com/css?family=Khula:400,300|Roboto|Roboto+Slab:400,700' rel='stylesheet' type='text/css' />
          <link rel="stylesheet" href="/bootstrap/dist/css/bootstrap.min.css" />
          <link rel="stylesheet" href="/font-awesome/css/font-awesome.css" />
          <link rel="stylesheet" href="/chartist/dist/chartist.min.css" />
          <link rel="stylesheet" href="/style.css" />
          <script src="/chartist/dist/chartist.min.js"></script>
          <script src="/script.js" async></script>
        </head>

        <Pages path={activePath} onBeforeNavigation={this.onBeforeNavigation} >
          <Page path="/"       handler={HomeIndex}  {...this.props} />
          <Page path="/sales"  handler={SalesIndex} {...this.props} />

          <NotFound handler={_404} />
        </Pages>
      </html>
    );
  }
});

module.exports = routes;