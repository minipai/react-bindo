/** @jsx React.DOM */

var React = require('react');
var Layout = require('../layout/base.jsx');

var Signup = React.createClass({

  render: function() {
    return (
      <Layout {...this.props}>
        <div className="panel panel-default login-panel">
          <div className="panel-heading">
            <h3 className="panel-title text-center">註冊帳號</h3>
          </div>
          <div className="panel-body">
            <form action="/signup" method="post" className="login-form">
              <div className="form-group">
                <label for="usernameInput">帳號</label>
                <input type="text" name="username" id="usernameInput" placeholder="限英數字，至少6個字元以上" className="form-control" />
              </div>
              <div className="form-group">
                <label for="passwordInput">密碼</label>
                  <input type="password" name="password" id="passwordInput" placeholder="至少6個字元以上" className="form-control" />
              </div>
              <div className="form-group">
                <label for="confirmInput">確認密碼</label>
                  <input type="password" name="confirm" id="confirmInput" placeholder="再輸入一次密碼" className="form-control" />
              </div>
              <div className="form-group">
                <button className="btn btn-primary btn-block">註冊</button>
              </div>
            </form>
          </div>
        </div>
      </Layout>
    );
  }
});

module.exports = Signup;


