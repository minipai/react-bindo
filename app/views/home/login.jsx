var React = require('react');
var Layout = require('../layout/base.jsx');


var Login = React.createClass({

  render: function() {
    return (
      <Layout {...this.props}>
        <div className="panel panel-default login-panel">
          <div className="panel-heading">
            <h3 className="panel-title text-center">帳號登入</h3>
          </div>
          <div className="panel-body">
            <form action="/login" method="post" className="login-form ng-pristine ng-valid">
              <div className="form-group">
                <label for="usernameInput">帳號</label>
                <input type="text" name="username" id="usernameInput" className="form-control"/>
              </div>
              <div className="form-group">
                <label for="passwordInput">密碼</label>
                <input type="password" name="password" id="passwordInput" className="form-control" />
              </div>
              <div className="form-group">
                <button className="btn btn-primary btn-block">登入</button>
              </div>
            </form>
          </div>
        </div>
      </Layout>
    );
  }
});

module.exports = Login;


