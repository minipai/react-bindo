var React    = require('react');
var Layout   = require('../layout/base.jsx');
var Main     = require('../layout/main.jsx');
var Widget   = require('../component/widget.jsx');
var _ = require('lodash');
var Home = React.createClass({

  render: function() {
    var widgets = this.props.widgets;

    var mainWidgets = _.take(widgets, 2);
    var secWidgets = _.drop(widgets, 2);

    return (
      <Layout {...this.props}>
        <header className="home-header">
          <div className="container"><h1> Business overview</h1></div>
        </header>
        <Main>
          <div className="home-dashboard">
           {
             mainWidgets.map( (widget)=> <Widget {...widget} /> )
           }
          </div>
          <div className="home-dashboard-sec">
            {
              secWidgets.map( (widget)=> <Widget {...widget} /> )
            }
          </div>
        </Main>
      </Layout>
    );
  }
});

module.exports = Home;

