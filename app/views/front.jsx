var React = require('react');

var Page = require('./page.jsx');
var request = require('superagent');

request
  .get(location.href)
  .set('Accept', 'application/json')
  .end(function(err, result){
    if (result.error) {
      console.error(this.props.url, result.error.message);

    } else {

      React.render( <Page {...result.body} />, document);
    }
  });

