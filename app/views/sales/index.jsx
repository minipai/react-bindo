var _ = require('lodash');

var React = require('react');
var Layout = require('../layout/base.jsx');
var Main     = require('../layout/main.jsx');
var Toolbar  = require('../layout/_toolbar.jsx');

var Chart = require('../component/chart.js');
var SimpleTable = require('react-simple-table');

var loadChart = require('../../stores/sales.js').loadChart;
var salesStore = require('../../stores/sales.js').salesStore;

var ReactCSSTransitionGroup = React.addons.CSSTransitionGroup;

var Home = React.createClass({
  getInitialState: function() {

    this.props.salesSummary = this.props.salesSummary || {type: '', data: []};
    return {
      chartType: this.props.salesSummary.type,
      chartData: this.props.salesSummary.data
    };
  },

  componentWillReceiveProps: function(nextProps) {
    if(nextProps.salesSummary) {
      this.setState({
        chartId: nextProps.salesSummary.id,
        chartType: nextProps.salesSummary.type,
        chartData: nextProps.salesSummary.data

      });

    }

  },

  componentDidMount: function() {
    var _this = this;

    salesStore.listen(function(result) {
      if (_this.isMounted()) {
        _this.setState({
          chartId: result.id,
          chartType: result.type,
          chartData: result.data

        });
      }
    });
  },
  updateChart: function(type) {
    loadChart(type);
  },
  render: function() {

    var topSalesColumns = ['name', 'price', 'sales'];
    var topSalesData = this.props.topSales || [];
    var bestSalesData = this.props.bestSales || [];
    var worstSalesData = this.props.worstSales || [];
    var chartData = this.state.chartData || [];

    var barChartData = {
      labels: _.map(chartData, 'label'),
      series: [
        _.map(chartData, 'revenueIndoor'),
        _.map(chartData, 'revenueWeb'),
      ]
    };
    var chartOptions = {
      stackBars: true,
    };
    var navCx = (type) => this.state.chartType === type ? 'active': '';
    var title = (type) => { return {d: 'Day', w: 'Week', m: 'Month', q: 'Quarter', y: 'YTD'}[type]; };
    return (
      <Layout {...this.props}>
        <Toolbar title="Sales">
        <h3>Sales</h3>
        </Toolbar>

        <Main>
          <div className="sales-dashboard">
            <div className="dashboard-main widget">
              <div className="widget-header">
                <h3>Sales by {title(this.state.chartType)}</h3>
                  <ul className="nav nav-pills nav-pills-mini">
                    <li className={navCx('d')}>
                      <a onClick={this.updateChart.bind(this, 'd')}>Day</a></li>
                    <li className={navCx('w')}>
                      <a onClick={this.updateChart.bind(this, 'w')}>Week</a></li>
                    <li className={navCx('m')}>
                      <a onClick={this.updateChart.bind(this, 'm')}>Month</a></li>
                    <li className={navCx('q')}>
                      <a onClick={this.updateChart.bind(this, 'q')}>Quarter</a></li>
                    <li className={navCx('y')}>
                      <a onClick={this.updateChart.bind(this, 'y')}>YTD</a></li>

                  </ul>
                <div className="clear"></div>
              </div>
              <div className="sales-chart">
                <div className="chart-revenue">
                  <ReactCSSTransitionGroup transitionName="chartTrans" component="div" className="chartTransWrapper">
                    <Chart type="Bar" data={barChartData} options={chartOptions} key={this.state.chartId} />
                  </ReactCSSTransitionGroup>
                </div>
                <table className="table table-revenue">
                  <thead>
                    <tr>
                      <th></th>
                      {
                        chartData.map( sale => <td>{sale.label}</td> )
                      }
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <th>Revenue(web)</th>
                      {
                        chartData.map( sale => <td>${sale.revenueWeb}</td> )
                      }
                    </tr>
                    <tr>
                      <th>Revenue(in-door)</th>
                      {
                        chartData.map( sale => <td>${sale.revenueIndoor}</td> )
                      }
                    </tr>
                    <tr>
                      <th># orders</th>
                      {
                        chartData.map( sale => <td>{sale.orders}</td> )
                      }
                    </tr>
                    <tr>
                      <th>Avg. Rev</th>
                      {
                        chartData.map( sale => <td>${((sale.revenueIndoor+sale.revenueWeb)/sale.orders).toFixed(2)}</td> )
                      }
                    </tr>
                  </tbody>
                </table>
              </div>
            </div>
            <div className="dashboard-sider">
              <div className="widget">
                <div className="widget-header single-line">
                  <h4>Top Sales Department</h4>
                </div>
                <SimpleTable className="table" columns={topSalesColumns} data={topSalesData}></SimpleTable>
                <a href="">
                  <div className="widget-more">
                    more data
                  </div>
                </a>
              </div>
              <div className="widget">
                <div className="widget-header single-line">
                  <h4>Best Sales Product</h4>
                </div>
                <SimpleTable className="table" columns={topSalesColumns} data={bestSalesData}></SimpleTable>
                <a href="">
                  <div className="widget-more">
                    more data
                  </div>
                </a>

              </div>
              <div className="widget">
                <div className="widget-header single-line">
                  <h4>Worst Sales Product</h4>
                </div>
                <SimpleTable className="table" columns={topSalesColumns} data={worstSalesData}></SimpleTable>
                <a href="">
                  <div className="widget-more">
                    more data
                  </div>
                </a>

              </div>
            </div>

          </div>
        </Main>
      </Layout>
    );
  }
});

module.exports = Home;

