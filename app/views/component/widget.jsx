var React = require('react');

var Chart = require('../component/chart.js');

var Widget = React.createClass({
  render: function() {

    var history = this.props.history;

    var percentage = (history[0] - history[1]) / history[1] * 100;
    var arrowColorClass = percentage > 0 ? 'text-success': 'text-danger';
    var arrowIconClass = percentage > 0 ? 'fa-long-arrow-up': 'fa-long-arrow-down';

    var salesData = {
      labels: ['','','','','','','','','','' ],
      series: [history]
    };

    return (
      <div className={'widget home-widget home-widget-'+this.props.category}>
        <div className="widget-header size-l text-center">
          {this.props.title}
        </div>
        <div className="widget-number">
          {this.props.number}
        </div>

        <Chart type="Line" data={salesData} />

        <div className="widget-footer clearfix">
          <span className={'pull-left '+arrowColorClass}>
            <i className={'fa '+arrowIconClass}></i> {Math.abs(percentage.toFixed())} %
          </span>

          <span className="pull-right">
            <i className="fa fa-clock-o"></i> {this.props.updated}
          </span>

        </div>
      </div>
    );
  }
});

module.exports = Widget;
