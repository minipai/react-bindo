var _ = require('lodash');
var moment = require('moment');
var Sales =  require('../services/sales');

module.exports = {
  index: function(req, res, next){
    var data = req.data;

    var start = moment();
    var dates = _.times( 6, (n => start.subtract(1, 'months').clone()) ).reverse();

    data.topSales = Sales.topSales();
    data.bestSales = Sales.bestSales();
    data.worstSales = Sales.worstSales();


    data.salesSummary = {
      type: 'm',
      data:  Sales.salesSummary(dates, 'm')
    };

    next();
  },

  chart: function(req, res, next) {
    var data = req.data;
    var type = req.params.type;

    var start = moment();

    switch (type) {
      case "d":
        var dates = _.times( 6, (n => start.subtract(1, 'days').clone()) ).reverse();
        break;
      case "w":
        var dates = _.times( 6, (n => start.subtract(1, 'weeks').clone()) ).reverse();
        break;
      case "m":
        var dates = _.times( 6, (n => start.subtract(1, 'months').clone()) ).reverse();
        break;
      case "q":
        var dates = _.times( 6, (n => start.subtract(1, 'quarters').clone()) ).reverse();
        break;
      case "y":
        var dates = _.times( 6, (n => start.subtract(1, 'years').clone()) ).reverse();
        break;

    }

    data.salesSummary = {
      id: Date.now(),
      type: type,
      data:  Sales.salesSummary(dates, type)
    };
    next();
  }
};