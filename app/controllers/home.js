var faker = require('faker');
var moment = require('moment');
var _ = require('lodash');

module.exports = {
  index: function(req, res, next){
    var data = req.data;

    data.widgets = [
      {category: 'sales', title: "Today's Sales" },
      {category: 'sales', title: "Today’s Orders" },
      {category: 'inventory', title: "Sales / Order" },
      {category: 'inventory', title: "Invoices Outstanding" },
      {category: 'customer', title: "Total Stock Value" },
      {category: 'customer', title: "Products to Reorder" },
      {category: 'product', title: "Outstanding P/Os" },
      {category: 'product', title: "Gross Margin" },
      {category: 'emplo', title: "Today’s Visits" },
      {category: 'emplo', title: "New Member" },
    ];

    data.widgets.forEach( widget => widget.number = faker.random.number(500) );
    data.widgets.forEach( widget => widget.updated = moment(faker.date.recent()).format('l LT') );
    data.widgets.forEach( widget => widget.history = _.times(10, ()=> faker.random.number(500)) );

    next();

  },

  signup: function(req, res, next) {
    next();
  },

  login: function(req, res, next){
    next();
  },

};