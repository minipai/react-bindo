var Reflux = require('reflux');

var reload = Reflux.createAction();

var agent = require('superagent');

var pageStore = Reflux.createStore({
    init: function() {
      this.data = {};
      this.listenTo(reload, this.reload);
    },

    reload: function() {
      this.fetch();
    },

    fetch: function() {
      console.info('before fetch, currentData', this.data);
      var _this = this;

      agent
        .get(location.href)
        .set('Accept', 'application/json')
        .end(function(err, result){
          if (result.error) {
            console.error(this.props.url, result.error.message);

          } else {
            _this.data = result.body;
            _this.trigger(_this.data);
          }
        });
    }
});

exports.reload = reload;
exports.pageStore = pageStore;
