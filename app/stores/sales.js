var Reflux = require('reflux');

var loadChart = Reflux.createAction();

var agent = require('superagent');

var salesStore = Reflux.createStore({
    init: function() {
      this.data = {};
      this.listenTo(loadChart, this.loadChart);
    },

    loadChart: function(type) {
      var _this = this;

      agent
        .get('/sales-chart/'+type)
        .set('Accept', 'application/json')
        .end(function(err, result){
          if (result.error) {
            console.error(result.error.message);

          } else {
            _this.trigger(result.body.salesSummary);
          }
        });
    }
});

exports.loadChart = loadChart;
exports.salesStore = salesStore;
