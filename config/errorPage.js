module.exports = function errorPage(err, req, res, next) {
  res.statusCode = err.status || 500;
  if (res.statusCode < 400) res.statusCode = 500;
  console.error(err.stack);
  var accept = req.headers.accept || '';
  // html
  if (~accept.indexOf('html')) {
    res.end("Sorry! No time for fancy ERROR PAGE");
  // json
  } else if (~accept.indexOf('json')) {
    var error = { message: err.message, stack: err.stack };
    for (var prop in err) error[prop] = err[prop];
    var json = JSON.stringify({ error: error });
    res.setHeader('Content-Type', 'application/json');
    res.end(json);
  // plain text
  } else {
    res.setHeader('Content-Type', 'text/plain');
    res.end(err.stack);
  }
};


