var Page = require('../app/views/page.jsx');
var React = require('react');
var cache = require('memory-cache');


module.exports = function() {
  return function(req, res, next){
    var data = cache.get(req.path) || req.data;

    data.path = req.path;
    data.url = req.url;
    data = JSON.parse(JSON.stringify(data));

    cache.put(req.path, data, 1000);

    res.format({
      'text/html': function(){
        if(res.statusCode == 303) {
          res.redirect(req.session.history[1]);
          return;
        }

        try {
          var app = Page(data);
          var markup = React.renderToString(app);
          res.send(markup);

        } catch(err) {
          return next(err);
        }
      },

      'application/json': function(){
        cache.del(req.path);

        res.json(data);
      },

      'default': function() {
        res.status(406).send('Not Acceptable');
      }
    });

  };
};


