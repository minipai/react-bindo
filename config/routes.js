var express   = require('express');

var home      = require('../app/controllers/home');
var sales     = require('../app/controllers/sales');

var router = express.Router();

router.get(  '/',                  home.index );
router.get(  '/sales',             sales.index );
router.get(  '/sales-chart/:type', sales.chart );

module.exports = router;

