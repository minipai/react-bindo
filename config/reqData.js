module.exports = function() {
  return function(req, res, next) {
    console.info('Path:', req.method ,req.path);

    req.data = {};
    req.data.errors = {};

    // Fake account data for demo
    req.data.account = {
      username: 'Bindor'
    };
    next();
  };
};
