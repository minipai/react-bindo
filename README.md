# README #

### About ###

Source code of my work about Bindo Test Assignment

view live demo at https://bindo-test.herokuapp.com/

### Install and run ###

Install dependencies:

* npm install
* bower install

Run `npm start` and the site will run on `http://localhost:4000/`

2 path `/` and `/sales` are available.

### Highlight ###

* isomorphic Javascript: HTML is server rendered, then enhance by React
* isomorphic Javascript: site is functional with JavaScript off, except chart
* random generated fake data
* faded chart under dashboard number

### Stuffs not included ###

Some technique I know, but not used in this project, because its overkill for a demo.

* build tool: Gulp
* optimize assets
* IE compatibility check ( I don't own a Windows PC)
* RWD (supports iPad and larget)

### Thank you ###

Questions welcome : minipai@gmail.com