require('node-jsx').install({harmony: true});

var express  = require('express'),
    app      = express();

var cookieParser = require('cookie-parser');
var bodyParser   = require('body-parser');
var errorhandler = require('errorhandler');

var flash      = require('connect-flash');

var renderView = require('./config/renderView');
var reqData    = require('./config/reqData');
var errorPage  = require('./config/errorPage');

app.set('env', process.env.NODE_ENV);

app.use( express.static(__dirname + '/public') );
app.use( express.static(__dirname + '/bower_components') );

app.use( bodyParser.json() );
app.use( bodyParser.urlencoded({ extended: false }) );
app.use( cookieParser() );

app.use( flash() );

app.use( reqData () );

app.use('/', require('./config/routes'));
app.use( renderView() );


app.get('/*', function(req, res, next){
    return next( new Error(404) );
});
if ('production' == app.get('env')) {
  app.use(errorPage());
}
else {
  app.use(errorhandler());
}


module.exports = app;